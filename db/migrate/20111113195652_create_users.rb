class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, 	:null => false,
      									:limit => 50
      t.string :email, 	:null => false,
      									:default => "" 

      t.string :salt
      t.string :encrypted_password
      t.timestamps
    end

    add_index :users, :email, :unique => true
  end
end
